---
layout: page
title: "Delhi - 5th Jan to 9th Jan"
permalink: /delhi/
---

Schedule not fixed yet. Please [contact us](/contact/).

### 5th Jan

  * **Timings**: 3:00 PM to 6:00 PM
  * **Venue**: Room 229, School of International Studies II, Jawaharlal Nehru University (JNU), New Delhi

### 6th Jan

    <!-- Add details here -->

### 7th Jan

    <!-- Add details here -->

### 8th Jan

    <!-- Add details here -->

### 9th Jan

    <!-- Add details here -->

